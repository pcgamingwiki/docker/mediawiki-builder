FROM rockylinux:8.5

RUN yum update -y \
 && yum install -y https://rpms.remirepo.net/enterprise/remi-release-8.rpm epel-release\
 && yum module enable -y php:remi-7.4 \
 && yum install -y\
    wget\
    git\
    unzip\
    nginx\
    php-fpm\
    php-cli\
    php-gd\
    php-intl\
    php-json\
    php-mbstring\
    php-mysqlnd\
    php-opcache\
    php-pecl-apcu\
    php-pecl-memcached\
    php-pecl-zip\
    php-xml\
    ImageMagick\
    librsvg2-tools\
    python3-pip\
  && yum clean all
RUN wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.5/dumb-init_1.2.5_x86_64 && chmod +x /usr/local/bin/dumb-init
RUN wget -cO - https://getcomposer.org/composer-1.phar > /usr/local/bin/composer && chmod +x /usr/local/bin/composer
RUN pip3 install honcho

ADD --chown=nginx:nginx mediawiki /opt/mediawiki/w/

RUN mkdir -p /run/nginx /run/php-fpm/{session,wsdlcache,opcache} -m 770 && chown -R nginx:root /var/log/php-fpm /var/log/nginx /run/nginx /run/php-fpm
ADD nginx/ /etc/nginx/
ADD php-fpm/php-fpm.conf /etc/
ADD Procfile /container/Procfile

ENTRYPOINT ["/usr/local/bin/dumb-init", "-c"]
CMD ["/usr/local/bin/honcho", "-f", "/container/Procfile", "start"]

USER nginx
#RUN composer config --working-dir=/opt/mediawiki/w/ --no-plugins allow-plugins.wikimedia/composer-merge-plugin true
RUN composer --working-dir=/opt/mediawiki/w/ update --no-dev

ONBUILD ADD composer.local.json /opt/mediawiki/w/
ONBUILD ADD extensions/ /opt/mediawiki/w/extensions/
ONBUILD ADD skins/ /opt/mediawiki/w/skins/
ONBUILD RUN composer --working-dir=/opt/mediawiki/w/ update --no-dev
